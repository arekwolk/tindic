use std::collections::HashMap;

use chrono::NaiveTime;
use chrono::Duration;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct SingleLog {
    pub time_start: String,
    pub time_end: String,
    pub title: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SingleSummary {
    title: String,
    pub duration_minutes: i64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Diary {
    diary: HashMap<String, Vec<SingleLog>>
}

impl Diary {
    pub fn new() -> Diary {
        Diary {
            diary: HashMap::new()
        }
    }

    pub fn add(&mut self, log: SingleLog) {
        let date = today_string();
        if self.diary.contains_key(&date) {
            self.diary.get_mut(&date).unwrap().push(log);
        } else {
            let logs = vec![log];
            self.diary.insert(date, logs);
        }
    }

    pub fn summarize_date(&self, date: String) -> Option<HashMap<String, SingleSummary>> {
        match self.diary.get(&date) {
            None => Option::None,
            Some(logs) => Some(logs.iter().fold(HashMap::new(), |mut sum, log| {
                match sum.get_mut(&log.title) {
                    Some(single_sum) => {
                        let diff = calculate_duration_minutes(&log.time_start, &log.time_end);
                        single_sum.duration_minutes = single_sum.duration_minutes + diff;
                    }
                    None => {
                        let single_summary = SingleSummary {
                            title: log.title.clone(),
                            duration_minutes: calculate_duration_minutes(&log.time_start, &log.time_end),
                        };
                        sum.insert(log.title.clone(), single_summary);
                    }
                };

                sum
            }))
        }
    }

    pub fn list_logs(&self, date: &String) -> Option<&Vec<SingleLog>> {
        self.diary.get(date)
    }
}


fn calculate_duration_minutes(time_start: &String, time_end: &String) -> i64 {
    let naive_time_start = NaiveTime::parse_from_str(time_start, "%H:%M").unwrap();
    let naive_time_end = NaiveTime::parse_from_str(time_end, "%H:%M").unwrap();
    let duration: Duration = naive_time_end - naive_time_start;
    duration.num_minutes()
}

pub fn today_string() -> String {
    chrono::Local::today().naive_local().format("%d-%m-%Y").to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn diary_contains_two_logs_for_same_date() {
        let mut diary: Diary = Diary::new();

        let time_start = "10:50".to_string();
        let time_end = "13:20".to_string();
        let title = String::from("TND-01 make new log");
        let single_log_one = SingleLog { time_start, time_end, title };
        diary.add(single_log_one);

        let time_start = "14:50".to_string();
        let time_end = "14:50".to_string();
        let title = String::from("TND-01 make new log");
        let single_log_two = SingleLog { time_start, time_end, title };
        diary.add(single_log_two);
        let today = chrono::Local::today().naive_local().format("%d-%m-%Y").to_string();
        let log_vector = diary.diary.get(&today).unwrap();

        assert_eq!(log_vector.len(), 2);
    }

    #[test]
    fn test_diary_summary() {
        let time_start = "10:00".to_string();
        let time_end = "10:45".to_string();
        let title = String::from("Test title");
        let log_one = SingleLog { time_start, time_end, title: title.clone() };

        let time_start = "10:00".to_string();
        let time_end = "10:30".to_string();
        let log_two = SingleLog { time_start, time_end, title: title.clone() };

        let time_start = "10:00".to_string();
        let time_end = "10:40".to_string();
        let title_two = String::from("Test title 2");
        let log_three = SingleLog { time_start, time_end, title: title_two.clone() };

        let mut diary = Diary::new();
        diary.add(log_one);
        diary.add(log_two);
        diary.add(log_three);

        let today = chrono::Local::today().naive_local().format("%d-%m-%Y").to_string();
        let option = diary.summarize_date(today);
        assert!(option.is_some());

        let summary_map = option.unwrap();
        assert_eq!(summary_map.len(), 2);

        let summary_one = summary_map.get(&title).unwrap();
        assert_eq!(summary_one.duration_minutes, 75);

        let summary_two = summary_map.get(&title_two).unwrap();
        assert_eq!(summary_two.duration_minutes, 40);
    }

    #[test]
    fn test_diary_list_logs() {
        let time_start = "10:00".to_string();
        let time_end = "10:45".to_string();
        let title = String::from("Test title");
        let log_one = SingleLog { time_start, time_end, title: title.clone() };

        let time_start = "10:00".to_string();
        let time_end = "10:30".to_string();
        let log_two = SingleLog { time_start, time_end, title: title.clone() };

        let time_start = "10:00".to_string();
        let time_end = "10:40".to_string();
        let title_two = String::from("Test title 2");
        let log_three = SingleLog { time_start, time_end, title: title_two.clone() };

        let mut diary = Diary::new();
        diary.add(log_one);
        diary.add(log_two);
        diary.add(log_three);

        let today = chrono::Local::today().naive_local().format("%d-%m-%Y").to_string();

        let option = diary.list_logs(&today);
        assert!(option.is_some());

        let log_list = option.unwrap();
        assert_eq!(log_list.len(), 3);
    }
}
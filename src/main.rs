use std::fs::OpenOptions;
use std::io::{Read, Write};

use clap::{App, Arg, SubCommand};

use tindic::SingleLog;

fn main() {
    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open("diary.txt").unwrap();

    let mut contents = String::new();
    match file.read_to_string(&mut contents) {
        Ok(_size) => (),
        Err(e) => {
            eprintln!("Error while reading a file: {:?}", e);
        }
    };

    let mut diary: tindic::Diary = match serde_json::from_str(&contents) {
        Ok(diary) => diary,
        Err(e) => {
            eprintln!("Failed to read diary from content: {}", contents);
            eprintln!("Error while reading diary: {:?}", e);
            tindic::Diary::new()
        }
    };

    let arguments = App::new("tindic")
        .subcommands(vec![
            SubCommand::with_name("add")
                .about("Adds new log to the diary")
                .arg(Arg::with_name("time_start")
                    .required(true)
                    .help("Job start time in 24h format, e.g.: 10:35"))
                .arg(Arg::with_name("time_end")
                    .required(true)
                    .help("Job end time in 24h format, e.g.: 13:40"))
                .arg(Arg::with_name("job_description")
                    .required(true)
                    .help("Job description written inside of quotes, e.g.: \"TND-11 add description for arguments\"")),
            SubCommand::with_name("summary")
                .about("Display summary of today or given date.")
                .arg(Arg::with_name("date")
                    .required(false)
                    .default_value("today")
                    .help("Use DD-MM-YYYY format to display summary for the date")),
            SubCommand::with_name("list")
                .about("Display list of logs for today or given date")
                .arg(Arg::with_name("date")
                    .required(false)
                    .default_value("today")
                    .help("Use DD-MM-YYYY format to display log list for the date"))])
        .get_matches();


    let command = arguments.subcommand_name().unwrap();
    let (_sub_name, sub_args) = arguments.subcommand();
    let sub_args = sub_args.unwrap();
    match command {
        "add" => {
            let time_start = sub_args.value_of("time_start").unwrap().to_string();
            let time_end = sub_args.value_of("time_end").unwrap().to_string();
            let title = sub_args.value_of("job_description").unwrap().to_string();

            diary.add(SingleLog { time_start, time_end, title });
            println!("New log added!");
        }
        "summary" => {
            let date = sub_args.value_of("date").unwrap().to_string();

            let optional = if date.to_lowercase().eq("today") {
                diary.summarize_date(tindic::today_string())
            } else {
                diary.summarize_date(date.clone())
            };

            match optional {
                Some(summaries) => {
                    summaries.iter().for_each(|(title, summary)| {
                        println!("{}", title);
                        println!("\t{:.1} ({} minutes)", summary.duration_minutes as f64 / 60 as f64, summary.duration_minutes);
                    })
                }
                None => println!("No logs for date {}", date)
            }
        }
        "list" => {
            let date = sub_args.value_of("date").unwrap().to_string();

            let optional = if date.to_lowercase().eq("today") {
                diary.list_logs(&tindic::today_string())
            } else {
                diary.list_logs(&date)
            };

            match optional {
                Some(logs) => {
                    logs.iter().for_each(| log | {
                        println!("{} {} \"{}\"", log.time_start, log.time_end, log.title);
                    })
                }
                None => println!("No logs for date {}", date)
            }
        }
        _ => ()
    }

    let json = serde_json::to_string(&diary).unwrap();

    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .truncate(true)
        .open("diary.txt").unwrap();

    file.write_all(json.as_bytes()).unwrap();
}

mod cli {}